﻿/*
  MIT license
  by bernd@snowhow.info
*/


using System.IO.IsolatedStorage;
using System.IO;
using System.Diagnostics;
using WPCordovaClassLib.Cordova.JSON;
using ICSharpCode.SharpZipLib.Zip;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;

namespace WPCordovaClassLib.Cordova.Commands {
    public class Unzip : BaseCommand
    {
        public void unzip(string options)
        {
            string res = "{ \"done\": true }";
            string[] args = JsonHelper.Deserialize<string[]>(options);
            string src = args[0];
            string dest = args[1];
            string callbackId = args[2];
            Regex rgx = new Regex("^/+");
            string dst = rgx.Replace(dest, "");

            Debug.WriteLine("UNZIP: Result::" + src + " dst " + dst.Replace("///", ""));
            IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
            var zf = myIsolatedStorage.OpenFile(src, FileMode.Open, FileAccess.Read);
            
            using (ZipInputStream zip = new ZipInputStream(zf))
            {
                ZipEntry entry;
                //int size;
                long i = 0;
                long zipSize = zf.Length;
                
                byte[] buffer = new byte[4096];


                //DispatchFileTransferProgress(0, zipSize, callbackId);
                while ((entry = zip.GetNextEntry()) != null)
                {
                    i = i + 1;
                    
                    if (i%5 == 0)
                        DispatchFileTransferProgress(zip.Position, zipSize, callbackId);
                    if (!entry.IsFile)
                    {
                        continue;           // Ignore directories
                    }
                    string fullZipToPath = System.IO.Path.Combine(dst.Replace("///", ""), entry.Name);
                    string directoryName = System.IO.Path.GetDirectoryName(fullZipToPath);
                    //Debug.WriteLine("UNZIP: full: " + fullZipToPath + " dirname " + directoryName);
                    if (!myIsolatedStorage.DirectoryExists(directoryName))
                        myIsolatedStorage.CreateDirectory(directoryName);

                    using (var writer = myIsolatedStorage.OpenFile(fullZipToPath, FileMode.Create))
                    {
                        zip.CopyTo(writer);
                        //while (true)
                        //{
                        //    size = zip.Read(buffer, 0, buffer.Length);
                        //    if (size > 0)
                        //        writer.Write(buffer, 0, size);
                        //    else
                        //        break;
                        //}

                    }

                }
                DispatchFileTransferProgress(zipSize, zipSize, callbackId);
                DispatchCommandResult(new PluginResult(PluginResult.Status.OK, res));
            }

        }
        private void DispatchFileTransferProgress(long bytesLoaded, long bytesTotal, string callbackId, bool keepCallback = true)
        {
            // Debug.WriteLine("DispatchFileTransferProgress : " + callbackId);
            // send a progress change event
            FileTransferProgress progEvent = new FileTransferProgress(bytesTotal);
            progEvent.BytesLoaded = bytesLoaded;
            //if (bytesTotal > 0 && bytesLoaded > 0)
            progEvent.PercProgress = 100.0 / bytesTotal * bytesLoaded;
            PluginResult plugRes = new PluginResult(PluginResult.Status.OK, progEvent);
            plugRes.KeepCallback = keepCallback;
            plugRes.CallbackId = callbackId;
            DispatchCommandResult(plugRes, callbackId);
        }
        /// <summary>
        /// Represents a singular progress event to be passed back to javascript
        /// </summary>
        [DataContract]
        public class FileTransferProgress
        {
            /// <summary>
            /// Is the length of the response known?
            /// </summary>
            [DataMember(Name = "lengthComputable", IsRequired = true)]
            public bool LengthComputable { get; set; }
            /// <summary>
            /// amount of bytes loaded
            /// </summary>
            [DataMember(Name = "loaded", IsRequired = true)]
            public long BytesLoaded { get; set; }
            /// <summary>
            /// Total bytes
            /// </summary>
            [DataMember(Name = "total", IsRequired = false)]
            public long BytesTotal { get; set; }
            /// <summary>
            /// progress in percent
            /// </summary>
            [DataMember(Name = "progress", IsRequired = true)]
            public double PercProgress { get; set; }

            public FileTransferProgress(long bTotal = 0, long bLoaded = 0)
            {
                LengthComputable = bTotal > 0;
                BytesLoaded = bLoaded;
                BytesTotal = bTotal;
                PercProgress = 0.1;
            }
        }

    }

}
