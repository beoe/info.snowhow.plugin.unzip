/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

/*
 * most code taken from https://github.com/MobileChromeApps/zip
 */

#import "Unzip.h"
#import "SSZipArchive.h"

@implementation Unzip

- (void)unzip:(CDVInvokedUrlCommand*)command
{
  [self.commandDelegate runInBackground:^{

    NSURL *fileurl = [NSURL URLWithString:[command.arguments objectAtIndex:0]];
    NSString *file = fileurl.path;
    NSURL *desturl = [NSURL URLWithString:[command.arguments objectAtIndex:1]];
    NSString *destination = desturl.path;
    NSError *error;

    CDVPluginResult *result;
    if([SSZipArchive unzipFileAtPath:file toDestination:destination overwrite:YES password:nil error:&error]) {
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[destination stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    } else {
        NSLog(@"%@ - %@", @"Error occurred during unzipping", [error localizedDescription]);
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[@"Could not unzip archive" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
  }];
}

@end

