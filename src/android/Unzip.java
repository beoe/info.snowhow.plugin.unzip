/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package info.snowhow.plugin;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import android.content.Context;
import android.util.Log;
import android.os.Environment;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// import android.app.NotificationManager;
// import android.app.Notification;
// import androidx.core.app.NotificationCompat;
// import android.support.v4.app.NotificationCompat;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;

// import info.snowhow.R;

public class Unzip extends CordovaPlugin {

  private static final String LOG_TAG = "Unzip";
  public static final int BYTEBUFF = 4096;

  /**
   * Constructor.
   */
  public Unzip() {
  }

  /**
   * Executes the request and returns PluginResult.
   *
   * @param action            The action to execute.
   * @param args              JSONArray of arguments for the plugin.
   * @param callbackContext   The callback context used when calling back into JavaScript.
   * @return                  True when the action was valid, false otherwise.
   */
  public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
    if (action.equals("unzip")) {
      final String src = args.getString(0);
      final String dst = args.getString(1);
      // cordova.getActivity().runOnUiThread(new Runnable() {
      cordova.getThreadPool().execute(new Runnable() {
        public void run() {
          Unzip.this.unzip(src, dst, callbackContext);
          callbackContext.success();
        }
      });
    } else {
      return false;
    }
    return true;
  }

  /**
   * unzip file
   *
   * @param src_file       Source zip file
   * @param dst_path       Destination path
   */
  public void unzip(String src_file, String dst_path, CallbackContext cbctx) {
    Log.d(LOG_TAG, "Unzipping "+src_file+" to "+dst_path);
    Intent intent = new Intent ();
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    PendingIntent pend = PendingIntent.getActivity(cordova.getActivity(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    // NotificationCompat.Builder note = new NotificationCompat.Builder(
      // .setContentTitle(cordova.getActivity().getString(R.string.app_name))
    // NotificationCompat.Builder noteBuilder = new NotificationCompat.Builder(cordova.getActivity())
    //   .setContentTitle("snowhow map installation")
    //   .setSmallIcon(android.R.drawable.ic_menu_save)
    //   .setContentIntent(pend)
    //   .setContentText("Installation - 0%");

    // NotificationManager nm = (NotificationManager) cordova.getActivity().getSystemService(Activity.NOTIFICATION_SERVICE);
    // nm.notify(0, noteBuilder.build());
    try {
      int zipentries = 0;
      int currentzipentry = 0;
      int progress = 0;
      String zipfilename = completePath(src_file, false);
      String dirName = completePath(dst_path, true);
      File file = new File(zipfilename);
      BufferedOutputStream dest = null;
      BufferedInputStream is = null;
      ZipEntry entry;
      ZipFile zipfile;
      zipfile = new ZipFile(file);
      zipentries = zipfile.size();
      Enumeration<? extends ZipEntry> e = zipfile.entries();
      while (e.hasMoreElements()) {
        currentzipentry++;
        entry = (ZipEntry) e.nextElement();
        int count;
        byte data[] = new byte[BYTEBUFF];
        String fileName = dirName +"/"+ entry.getName();
        File outFile = new File(fileName);
        is = new BufferedInputStream(zipfile.getInputStream(entry), BYTEBUFF);
        if (entry.isDirectory()) {
          outFile.mkdirs();
        } else {
          FileOutputStream fos = new FileOutputStream(outFile);
          dest = new BufferedOutputStream(fos, BYTEBUFF);
          while ((count = is.read(data, 0, BYTEBUFF)) != -1) {
            dest.write(data, 0, count);
          }
          dest.flush();
          dest.close();
          is.close();
        }
        int newProgress = (int) (100/(float)zipentries*currentzipentry);
        if (newProgress != progress) {
          progress = informProgress(zipentries, newProgress, zipfilename, cbctx);
          // if ((progress % 2) == 0) {
            // noteBuilder.setProgress(100, progress, false);
            // noteBuilder.setContentText("Installation - "+progress+"%");
            // nm.notify(0, noteBuilder.build());
          // }
        }
      }
      // noteBuilder.setProgress(0, 0, false);
      // noteBuilder.setContentText("Installation complete");
      // nm.notify(0, noteBuilder.build());
      // nm.cancel(0);
      Log.d(LOG_TAG, "unzipping finished");
    } catch (ZipException e1) {
      Log.d(LOG_TAG, "error"+e1);
    } catch (IOException e1) {
      Log.d(LOG_TAG, "error"+e1);
    } catch (JSONException e1) {
      Log.d(LOG_TAG, "error"+e1);
    } catch (InterruptedException e1) {
      Log.d(LOG_TAG, "error"+e1);
    }
  }

  private String completePath(String p, boolean isDir) {
    Log.d(LOG_TAG, "incoming string is "+p);
    String dn = "";
    if (p.startsWith("file:")) {
      try {
        URL url = new URL(p);
        dn = url.getFile();
      } catch (MalformedURLException e) {
        dn = p;
      }
    } else if (p.matches("^/")) {
      dn = p;
    } else {
      dn = Environment.getExternalStorageDirectory()+"/"+p;
    }
    File data = new File(dn);
    if (isDir && !data.isDirectory()) {
      Log.d(LOG_TAG, "creating directory "+data);
      data.mkdirs();
    }
    return data.toString();
  }

  private int informProgress(int zipentries, int progress, String fileName, CallbackContext cbctx) throws InterruptedException, JSONException {
    if ((progress % 5) == 0) {
      Log.d(LOG_TAG, "updating progress ... "+progress);
      JSONObject obj = new JSONObject();
      obj.put("status", 0);
      obj.put("entries", zipentries);
      obj.put("file", fileName);
      obj.put("progress", progress);
      PluginResult res = new PluginResult(PluginResult.Status.OK, obj);
      res.setKeepCallback(true);
      cbctx.sendPluginResult(res);
      Thread.sleep(100);
    }
    return progress;
  }
}
