var cordova = require('cordova'),
    UnzipPlugin = require('./UnzipPlugin');
    // zip = require('./zip.js');
    // var uz = require('unzip');

module.exports = {

  unzip: function (successCallback, errorCallback, opts) {
      console.log("running plugin unzip ... ", opts);
      var src = opts[0],
          dst = opts[1];

      function upZipFile(zipFile, unzipFolder) {
          try {
              // console.log("Upziping file: " + zipFile.displayName + "...");
              ZipHelperWinRT.ZipHelper.unZipFileAsync(zipFile, unzipFolder).done(function () {
                  console.log("Unzip file '" + zipFile+ "' successfully");
              });
          }
          catch (err) {
              console.log("Failed to unzip file ...");
              errorCallback({ done: false, error: err });
          
          }
      }
      upZipFile(src, dst);
    successCallback({ done: true });
  }

};

require("cordova/exec/proxy").add("UnzipPlugin", module.exports);

