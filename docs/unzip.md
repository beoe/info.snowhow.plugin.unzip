---
license: Licensed to the Apache Software Foundation (ASF) under one
         or more contributor license agreements.  See the NOTICE file
         distributed with this work for additional information
         regarding copyright ownership.  The ASF licenses this file
         to you under the Apache License, Version 2.0 (the
         "License"); you may not use this file except in compliance
         with the License.  You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

         Unless required by applicable law or agreed to in writing,
         software distributed under the License is distributed on an
         "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         KIND, either express or implied.  See the License for the
         specific language governing permissions and limitations
         under the License.
---

unzip
====================

Unzip a specified file to a location

    var u = new FileUnzip();
    u.onprogress = function(prog) {
      console.log("done: "+prog.progress);
    };
    u.unzip(src_file, target_path, success, error);


Supported Platforms
-------------------

- Android
- iPhone

Quick Example
-------------

    // unzip file a.zip to sdcard
    //
    var u = new FileUnzip();
    u.onprogress = function(prog) {
      console.log("done: "+prog.progress);
    };
    u.unzip(src_file, target_path);

Full Example
------------
    
    <!DOCTYPE html>
    <html>
      <head>
        <title>Unzip example</title>

        <script type="text/javascript" charset="utf-8" src="cordova-x.x.x.js"></script>
        <script type="text/javascript" charset="utf-8">

        // Wait for Cordova to load
        //
        document.addEventListener("deviceready", onDeviceReady, false);

        // Cordova is ready
        //
        function onDeviceReady() {
            // Empty
        }
    
        // TODO
        </script>
      </head>
      <body>
        <p><a href="#" onclick="snowhow.plugin.unzip('a.zip', '/sdcard/snowhow'); return false;">Unzip</a></p>
      </body>
    </html>

